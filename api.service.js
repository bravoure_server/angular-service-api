(function () {
    'use strict';

    function httpService($http) {

        var httpService = {};

        httpService.getContent = function (url) {
            return $http.jsonp(url);
        };

        return httpService;

    }

    httpService.$inject = ['$http'];

    function API_Service($resource) {
        return function (url, params, is_array) {

            is_array = (is_array == undefined) ? true : is_array;

            var data = $resource(url, params, {
                get: {
                    method: 'GET',
                    isArray: is_array,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                },
                get_no_array: {
                    method: 'GET',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                },
                save: {
                    method: 'POST',
                    isArray: is_array,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                },
                save_no_array: {
                    method: 'POST',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                },
                delete: {
                    method: 'DELETE',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                },
                put: {
                    method: 'PUT',
                    isArray: false,
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    }
                }
            });

            return data;
        };
    }

    API_Service.$inject = ['$resource'];

    angular.module('bravoureAngularApp')
        .factory('httpService', httpService)
        .factory('API_Service', API_Service);

})();
